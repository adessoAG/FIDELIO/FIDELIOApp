/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */package de.persoapp.android.fidelio;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import de.persoapp.android.activity.AuthenticateActivity;

/**
 * Android activity providing FIDELIO functionality alternatively to Google Authenticator.
 * This activity is usually called by Chrome and the Google reference implementation of u2f-api.js.
 *
 */
public class FIDOActivity extends Activity {

    private static final String TAG                 = "FIDELIO";

    private static final String ACTION_GOOGLE       = "com.google.android.apps.authenticator.AUTHENTICATE";
    private static final String ACTION_FIDELIO      = "de.bund.bsi.fidelio.AUTHENTICATE";

    private static final String FIDO_REQUEST        = "request";
    private static final String FIDO_RESPONSE       = "resultData";

    private final FIDELIO       fidelio             = new FIDELIO();

    private static final String TR03124_URL         = "http://127.0.0.1:24727/eID-Client?tcTokenURL=";
    private static final String DEFAULT_FIDELIO_URL = "https://id1.vx4.eu/fidelio_dev/?data=";

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * Create this activity, call FIDELIO core and directly forward to eID activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        if (!intent.getAction().equals(ACTION_GOOGLE) && !intent.getAction().equals(ACTION_FIDELIO)) {
            Toast.makeText(FIDOActivity.this, "Unsupported Intent", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        final String request = intent.getStringExtra(FIDO_REQUEST);
        if (request == null) {
            Log.e(TAG, "Request missing");
            finish();
            return;
        }

        try {
            byte[] sigHash = null;
            for (final Signature sig : getPackageManager().getPackageInfo(getCallingPackage(), PackageManager.GET_SIGNATURES).signatures) {
                sigHash = MessageDigest.getInstance("SHA-256").digest(sig.toByteArray());
                Log.i(TAG, "Signature : " + new BigInteger(1, sigHash).toString(16));
            }

            String referrer = intent.getStringExtra("referrer");
            if (referrer == null || referrer.isEmpty()) {
                referrer = "android:apk-key-hash:" + Base64.encodeToString(sigHash, Base64.URL_SAFE | Base64.NO_PADDING | Base64.NO_WRAP);
            }

            final String fidelioURL = getSharedPreferences("FIDELIO", MODE_PRIVATE).getString("serverURL", DEFAULT_FIDELIO_URL);
            final Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(TR03124_URL + fidelioURL + this.fidelio.createCTAP(referrer, request)));
            i.setClass(getBaseContext(), AuthenticateActivity.class);
            i.putExtra(Intent.EXTRA_RETURN_RESULT, Boolean.valueOf(true));

            startActivityForResult(i, 1);
        } catch (final Exception e) {
            Toast.makeText(FIDOActivity.this, "Error identifying caller.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Get response of eID activity and parse it using FIDELIO core.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK  && data.getData() != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final String finalResponse = FIDOActivity.this.fidelio.parseResponse(data.getData().toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (finalResponse != null) {
                                final Intent intent = getIntent();
                                intent.putExtra(FIDO_RESPONSE, finalResponse);
                                setResult(Activity.RESULT_OK, intent);
                            }
                            finish();
                        }
                    });
                }
            }).start();
        } else {
            finish();
        }
    }

    /**
     * At the moment this isn't really seen in the wild. Keep it here for reference.
     *
     * @param aContext
     * @param callingUid
     * @return
     */

    private String getFacetID(final Context aContext, final int callingUid) {

        final String packageNames[] = aContext.getPackageManager().getPackagesForUid(callingUid);

        if (packageNames == null) {
            return null;
        }

        try {
            final PackageInfo info = aContext.getPackageManager().getPackageInfo(packageNames[0], PackageManager.GET_SIGNATURES);

            final byte[] cert = info.signatures[0].toByteArray();
            final InputStream input = new ByteArrayInputStream(cert);

            final CertificateFactory cf = CertificateFactory.getInstance("X509");
            final X509Certificate c = (X509Certificate) cf.generateCertificate(input);

            final MessageDigest md = MessageDigest.getInstance("SHA1");

            return "android:apk-key-hash:" + Base64.encodeToString(md.digest(c.getEncoded()), Base64.DEFAULT | Base64.NO_WRAP | Base64.NO_PADDING);
        } catch (final PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (final CertificateException e) {
            e.printStackTrace();
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

}
