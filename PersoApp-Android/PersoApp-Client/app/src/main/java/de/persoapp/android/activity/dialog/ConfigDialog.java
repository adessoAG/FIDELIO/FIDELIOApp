/**
 *
 * COPYRIGHT (C) 2010, 2011, 2012, 2013, 2014 AGETO Innovation GmbH
 *
 * Authors Christian Kahlo, Ralf Wondratschek
 *
 * All Rights Reserved.
 *
 * Contact: PersoApp, http://www.persoapp.de
 *
 * @version 1.0, 30.07.2013 13:50:47
 *
 *          This file is part of PersoApp.
 *
 *          PersoApp is free software: you can redistribute it and/or modify it
 *          under the terms of the GNU Lesser General Public License as
 *          published by the Free Software Foundation, either version 3 of the
 *          License, or (at your option) any later version.
 *
 *          PersoApp is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          Lesser General Public License for more details.
 *
 *          You should have received a copy of the GNU Lesser General Public
 *          License along with PersoApp. If not, see
 *          <http://www.gnu.org/licenses/>.
 *
 *          Diese Datei ist Teil von PersoApp.
 *
 *          PersoApp ist Freie Software: Sie können es unter den Bedingungen der
 *          GNU Lesser General Public License, wie von der Free Software
 *          Foundation, Version 3 der Lizenz oder (nach Ihrer Option) jeder
 *          späteren veröffentlichten Version, weiterverbreiten und/oder
 *          modifizieren.
 *
 *          PersoApp wird in der Hoffnung, dass es nützlich sein wird, aber OHNE
 *          JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *          Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN
 *          ZWECK. Siehe die GNU Lesser General Public License für weitere
 *          Details.
 *
 *          Sie sollten eine Kopie der GNU Lesser General Public License
 *          zusammen mit diesem Programm erhalten haben. Wenn nicht, siehe
 *          <http://www.gnu.org/licenses/>.
 *
 */
package de.persoapp.android.activity.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.persoapp.android.BuildConfig;
import de.persoapp.android.R;
import de.persoapp.android.activity.LicenseActivity;
import net.vrallev.android.base.util.Cat;

import javax.net.ssl.HttpsURLConnection;
import java.lang.reflect.Field;
import java.net.URI;

import static android.content.Context.MODE_PRIVATE;

/**
 * The <tt>ConfigDialog</tt> shows information about
 * the mobile eID-Client to the user.
 * 
 * @author Christian Kahlo
 */
public class ConfigDialog extends DialogFragment {

    private static final String DEFAULT_FIDELIO_URL = "https://id1.vx4.eu/fidelio_dev/?data=";

    @SuppressWarnings("ConstantConditions")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return onClickFIDELIOServer(this.getContext());
    }

    public Dialog onClickFIDELIOServer(final Context ctx) {
        final SharedPreferences pref = ctx.getSharedPreferences("FIDELIO", MODE_PRIVATE);
        final String fidelioURL = pref.getString("serverURL", "https://id1.vx4.eu/fidelio/?data=");

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("FIDELIO Server");

        final EditText input = new EditText(ctx);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
        alertDialogBuilder.setView(input);
        input.setText(fidelioURL);

        // set dialog message
        alertDialogBuilder.setMessage(fidelioURL).setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                // if this button is clicked, close current activity
                // MainActivity.this.finish();

                final String newURL = input.getText().toString().trim();
                if (!newURL.isEmpty() && newURL.toLowerCase().startsWith("https://") && !fidelioURL.equals(newURL)) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final URI serverURI = new URI(newURL);
                                final HttpsURLConnection uc = (HttpsURLConnection) serverURI.toURL().openConnection();
                                uc.connect();

                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("serverURL", newURL);
                                editor.apply();

                                dialog.dismiss();
                            } catch (final Exception e) {
                                Toast.makeText(ctx, "Error: " + e.getClass() + " " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }).start();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                // if this button is clicked, just close
                // the dialog box and do nothing
                dialog.cancel();
            }
        });

        // create alert dialog
        return alertDialogBuilder.create();
    }

}
