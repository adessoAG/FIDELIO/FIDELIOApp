/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */
package de.persoapp.android.fidelio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import javax.net.ssl.HttpsURLConnection;

import de.persoapp.core.util.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;
import co.nstant.in.cbor.CborBuilder;
import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborEncoder;
import co.nstant.in.cbor.builder.ArrayBuilder;
import co.nstant.in.cbor.builder.MapBuilder;

/**
 * FIDELIO core component for Android providing parsing, transforming and creating U2F requests into U2F responses.
 *
 * @author kahlo
 * @date 23.01.2017
 */
class FIDELIO {

    // generic constants

    private static final String TAG                     = "FIDELIO";

    private static final String JSON_TYPE               = "type";
    private static final String JSON_APPID              = "appId";
    private static final String JSON_CHALLENGE          = "challenge";
    private static final String JSON_REGISTERED_KEYS    = "registeredKeys";
    private static final String JSON_REGISTER_REQUESTS  = "registerRequests";
    private static final String JSON_KEYHANDLE          = "keyHandle";
    private static final String JSON_VERSION            = "version";
    private static final String JSON_REQUESTID          = "requestId";
    private static final String JSON_RESPONSEDATA       = "responseData";
    private static final String JSON_CLIENTDATA         = "clientData";
    private static final String JSON_SIGNATUREDATA      = "signatureData";
    private static final String JSON_REGISTRATIONDATA   = "registrationData";

    private static final String JSON_RESPONSE_MAGIC     = "typ";
    private static final String JSON_ORIGIN             = "origin";
    private static final String JSON_CID_PUBKEY         = "cid_pubkey";

    private static final String SIGN_REQUEST_TYPE       = "u2f_sign_request";
    private static final String SIGN_RESPONSE_TYPE      = "u2f_sign_response";

    private static final String REGISTER_REQUEST_TYPE   = "u2f_register_request";
    private static final String REGISTER_RESPONSE_TYPE  = "u2f_register_response";

    private static final String SIGN_RESPONSE_MAGIC     = "navigator.id.getAssertion";
    private static final String REGISTER_RESPONSE_MAGIC = "navigator.id.finishEnrollment";

    private static final String CID_UNAVAILABLE         = "unavailable";

    private static final String CID_UNUSED              = "unused";

    private static final String VERSION_U2F_V2          = "U2F_V2";

    private static final byte[] FIDELIO_TAG             = new byte[] { (byte) 0xF1, (byte) 0xDE, (byte) 0x11, (byte) 0x01 };

    private FIDOContext         mFIDOContext;

    /**
     * Keep track of current state with a private class keeping the context.
     *
     */
    private class FIDOContext {
        private final String       appId;
        private String             origin;
        private final List<byte[]> keyHandles;
        private final int          requestId;
        private final boolean      sign;
        private final byte[]       clientData;

        /**
         * Create a new FIDO U2F context of a referrer and a U2F request.
         *
         * @param referrer
         * @param u2fRequest
         * @throws JSONException
         * @throws IOException
         */
        private FIDOContext(final String referrer, final String u2fRequest) throws JSONException, IOException {
            final JSONObject json = new JSONObject(u2fRequest);
            switch (json.getString(JSON_TYPE)) {
                case REGISTER_REQUEST_TYPE:
                    this.sign = false;
                    break;
                case SIGN_REQUEST_TYPE:
                    this.sign = true;
                    break;
                default:
                    throw new IllegalStateException("Invalid request type");
            }


            this.appId = json.getString(JSON_APPID);
            try {
                this.origin = findFacetMatch(new URI(this.appId).toURL(), referrer);
            } catch(URISyntaxException e) {
                e.printStackTrace();
            }

            // no facet list
            if(this.origin == null) {
                String appIdString = referrer != null ? referrer : json.getString(JSON_APPID);
                try {
                    final int n = appIdString.indexOf("/", appIdString.indexOf("://") + 3);
                    if (n > 0) {
                        appIdString = appIdString.substring(0, n);
                    }
                    this.origin = appIdString;
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }

            this.requestId = json.getInt(JSON_REQUESTID);
            byte[] challenge = null;

            if (this.sign) {
                this.keyHandles = new ArrayList<>();
                challenge = Base64.decode(json.getString(JSON_CHALLENGE), Base64.URL_SAFE);
                final JSONArray array = json.getJSONArray(JSON_REGISTERED_KEYS);
                for (int i = 0; i < array.length(); i++) {
                    final JSONObject keyHandleItem = array.getJSONObject(i);
                    if (keyHandleItem.getString(JSON_VERSION).equals(VERSION_U2F_V2)) {
                        final byte[] keyHandle = Base64.decode(keyHandleItem.getString(JSON_KEYHANDLE), Base64.URL_SAFE);
                        if (keyHandle[0] == FIDELIO_TAG[0] && keyHandle[1] == FIDELIO_TAG[1] && keyHandle[2] == FIDELIO_TAG[2]
                                && keyHandle[3] == FIDELIO_TAG[3]) {
                            this.keyHandles.add(keyHandle);
                        }
                    }
                }
                if (this.keyHandles.size() <= 0) {
                    throw new IllegalStateException("No valid FIDELIO keyhandle in request.");
                }
            } else {
                this.keyHandles = null;
                final JSONArray array = json.getJSONArray(JSON_REGISTER_REQUESTS);
                for (int i = 0; i < array.length(); i++) {
                    final JSONObject registerItem = array.getJSONObject(i);
                    if (registerItem.getString(JSON_VERSION).equals(VERSION_U2F_V2)) {
                        challenge = Base64.decode(registerItem.getString(JSON_CHALLENGE), Base64.URL_SAFE);
                        break;
                    }
                }

                if (challenge == null) {
                    throw new IllegalStateException("No valid challenge in request.");
                }
            }
            // {"typ":"navigator.id.finishEnrollment","challenge":"OvLXp2-E0vuXq8soFF_0sw","origin":"https://eid-reroute.vx4.de","cid_pubkey":"unused"}
            // Returning from activity with result:
            // {"type":"u2f_register_response","requestId":1,"responseData":{"registrationData":"BQTV74UBvHmZ-holbVSqMFlgYQ1DaShYwlOswfT_vVVEY6R6UsiDsnG6-TrMLMbwfo8S55g2zZeyo_ANQEMuXNE7MPj7-cG47Mm3wxI2FBfOr_Mo0BVEMc2R_lr65FT5uhcp1ITaPnO_GEa7RLzPb2VBiDCCATkwgeCgAwIBAgIIIBcBJgAAAAEwCgYIKoZIzj0EAwIwFzEVMBMGA1UEAxMMVlg0IEZJRE8gQ0ExMB4XDTE3MDIwMzAxMjMwNloXDTI3MDIwMTAxMjMwNlowFjEUMBIGA1UEAwwLQkMgVTJGVjIgIzEwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATXJH7xp8Rq1FfxTNHWCmb2BUm-tOlpyE47d0jhynARtFZtwdzAyBVHCmFZ71ATzDhlN-8Y5NJqgArYzHA0tUrCoxcwFTATBgsrBgEEAYLlHAIBAQQEAwIEEDAKBggqhkjOPQQDAgNIADBFAiEA9q-xUQB7TUlS6NJaBUUfsUZ29wnDywCvmfUK02jkdjACIDJRUfpyd1PLrnX2ibkYorTjAKOtYlKSmPn5JGgatH4YMEUCIHwXZZ4FfA9_73w8b5RerYxKUQy8O5KcTJRGR5epVUYLAiEAq_XqtzrdpGEK-sp3u77bDKxz50dkELINbEfvucCb5BY","version":"U2F_V2","clientData":"eyJ0eXAiOiJuYXZpZ2F0b3IuaWQuZmluaXNoRW5yb2xsbWVudCIsImNoYWxsZW5nZSI6IlhuYVZXQUFBQUFDZ19UcmlZNzU0NHVXYUZ1ZXRUVFhzQnRuMlRpQ3VhLVVQQ3FoWW12aEpBUjBZOXlQaUdMWS1Gc0V4UlNfekhmR3hnNW1ualY5eUFhaVBwTzVuMWRtVEZBIiwib3JpZ2luIjoiaHR0cHM6XC9cL20uZmFjZWJvb2suY29tIiwiY2lkX3B1YmtleSI6InVuYXZhaWxhYmxlIn0"}}
            final JSONObject clientData = new JSONObject();
            clientData.put(JSON_RESPONSE_MAGIC, this.sign ? SIGN_RESPONSE_MAGIC : REGISTER_RESPONSE_MAGIC);
            clientData.put(JSON_CHALLENGE, Base64.encodeToString(challenge, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));

            clientData.put(JSON_ORIGIN, this.origin);

            clientData.put(JSON_CID_PUBKEY, CID_UNAVAILABLE);

            this.clientData = clientData.toString().getBytes("UTF-8");
        }

        private String getAppId() {
            return this.appId;
        }

        private List<byte[]> getKeyHandles() {
            return this.keyHandles;
        }

        private int getRequestId() {
            return this.requestId;
        }

        private boolean isSign() {
            return this.sign;
        }

        private byte[] getClientData() {
            return this.clientData;
        }
    }

    /**
     * Create a FIDELIO CTAP request from serialized U2F state information to be sent to the eService.
     *
     * @param referrer
     * @param u2fData
     * @return
     */
    String createCTAP(final String referrer, final String u2fData) {
        try {
            this.mFIDOContext = new FIDOContext(referrer, u2fData);

            final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            final CborBuilder cbor = new CborBuilder();

            final MapBuilder map = cbor.addMap().put(1, sha256.digest(this.mFIDOContext.getAppId().getBytes("UTF-8"))).put(2,
                    sha256.digest(this.mFIDOContext.getClientData()));

            if (this.mFIDOContext.isSign()) {
                final ArrayBuilder array = map.putArray(this.mFIDOContext.isSign() ? 3 : 5);
                for (final byte[] keyHandle : this.mFIDOContext.getKeyHandles()) {
                    array.add(keyHandle);
                }
                array.end();
            }

            map.end();

            final ByteArrayOutputStream baos = new ByteArrayOutputStream();

            // add leading command byte
            baos.write(this.mFIDOContext.isSign() ? 2 : 1);
            new CborEncoder(baos).encode(cbor.build());
            return Base64.encodeToString(baos.toByteArray(), Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING);
        } catch (final Exception e) {
            Log.e(TAG, "Error parsing request and creating eID Intent.", e);
        }

        return null;
    }

    /**
     * Fetch and parse the response from the eService into a JSON object and convert it to string.
     *
     * @param resultUri
     * @return
     */
    String parseResponse(final String resultUri) {
        try {

            // extract leading command byte
            final InputStream is = Util.openURL(new URI(resultUri).toURL()).getInputStream();
            final int status = is.read() & 0xFF;

            final Map<Object, Object> cborRes = CBORUtil.map(new CborDecoder(is).decodeNext());

            final byte[] fidelioResponse = (byte[]) cborRes.get("3");
            final byte[] chosenKey = (byte[]) cborRes.get("1");

            final JSONObject response = new JSONObject();
            final JSONObject responseData = new JSONObject();

            if (this.mFIDOContext.isSign()) {
                response.put(JSON_TYPE, SIGN_RESPONSE_TYPE);

                // responseData.put(JSON_KEYHANDLE,
                // Base64.encodeToString(mFIDOContext.getKeyHandles().get(chosenKey),
                // Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));

                responseData.put(JSON_KEYHANDLE, Base64.encodeToString(chosenKey, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));

                responseData.put(JSON_SIGNATUREDATA, Base64.encodeToString(fidelioResponse, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));
                responseData.put(JSON_VERSION, VERSION_U2F_V2);
                responseData.put(JSON_CLIENTDATA,
                        Base64.encodeToString(this.mFIDOContext.getClientData(), Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));
            } else {
                response.put(JSON_TYPE, REGISTER_RESPONSE_TYPE);
                responseData.put(JSON_REGISTRATIONDATA, Base64.encodeToString(fidelioResponse, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));
                responseData.put(JSON_VERSION, VERSION_U2F_V2);
                responseData.put(JSON_CLIENTDATA,
                        Base64.encodeToString(this.mFIDOContext.getClientData(), Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING));
            }

            response.put(JSON_REQUESTID, this.mFIDOContext.getRequestId());
            response.put(JSON_RESPONSEDATA, responseData);
            return response.toString();
        } catch (final Exception e) {
            Log.e(TAG, "Error parsing FIDELIO response", e);
        }

        return null;
    }

    /**
     * Find the trusted facet (if exists) of referrer and the given trusted facet list URL.
     *
     * @param facetURL
     * @param referrer
     * @return
     */
    private String findFacetMatch(final URL facetURL, final String referrer) {

        final FutureTask<String> ft = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                final HttpsURLConnection uc = (HttpsURLConnection) Util.openURL(facetURL);
                uc.setAllowUserInteraction(true);
                uc.connect();
                final InputStream is = uc.getInputStream();

                final java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
                final String result = s.hasNext() ? s.next() : "";
                is.close();

                return result;
            }
        });

        new Thread(ft).start();

        try {
            final JSONArray facetLists =  new JSONObject(ft.get()).getJSONArray("trustedFacets");
            for(int i = 0; i < facetLists.length(); i++)  {
                JSONObject facetList = facetLists.getJSONObject(i);
                final int vMajor = facetList.getJSONObject("version").getInt("major");
                final int vMinor = facetList.getJSONObject("version").getInt("minor");

                if(vMajor == 1 && vMinor == 0) {
                    JSONArray facetIDs = facetList.getJSONArray("ids");
                    for(int j = 0; j < facetIDs.length(); j++) {
                        Log.e(TAG, facetIDs.getString(j));
                        if(referrer.startsWith(facetIDs.getString(j))) {
                            return facetIDs.getString(j);
                        }
                    }
                }
            }

            throw new SecurityException("invalid facet");
        } catch (JSONException | InterruptedException | ExecutionException e) {
            Log.e(TAG, "no valid trusted facet list");
            e.printStackTrace();
        }

        return null;
    }
}
