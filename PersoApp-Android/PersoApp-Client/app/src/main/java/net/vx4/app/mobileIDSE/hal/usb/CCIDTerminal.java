/*
 * Copyright 2016-2017 adesso AG
 * Copyright 2011-2016 VX4.NET
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */
package net.vx4.app.mobileIDSE.hal.usb;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.hardware.usb.UsbRequest;
import android.util.Log;
import de.persoapp.core.card.CCID;
import de.persoapp.core.card.TransportProvider;
import de.persoapp.core.util.ArrayTool;
import de.persoapp.core.util.Hex;

/**
 * @author Christian Kahlo, ck@vx4.de
 *
 */
public class CCIDTerminal extends UsbBasicDevice implements CCID {

	/*
	 * slot states
	 */
	public static final byte SLOT_STATE_TIMEXT = (byte) 0x80;
	public static final byte SLOT_STATE_FAILED = (byte) 0x40;

	public static final byte SLOT_STATE_NO_ICC = (byte) 0x08;
	public static final byte SLOT_STATE_ICC_INACTIVE = (byte) 0x04;

	/*
	 * error codes
	 */
	public static final byte SLOT_ERROR_ABORTED = (byte) 0xFF;
	public static final byte SLOT_ERROR_ICC_MUTE = (byte) 0xFE;
	public static final byte SLOT_ERROR_PARITY = (byte) 0xFD;
	public static final byte SLOT_ERROR_OVERRUN = (byte) 0xFC;
	public static final byte SLOT_ERROR_HARDWARE = (byte) 0xFB;
	public static final byte SLOT_ERROR_BAD_ATR_TS = (byte) 0xF8;
	public static final byte SLOT_ERROR_BAD_ATR_TCK = (byte) 0xF7;
	public static final byte SLOT_ERROR_PROTO_UNSUP = (byte) 0xF6;
	public static final byte SLOT_ERROR_CLASS_UNSUP = (byte) 0xF5;
	public static final byte SLOT_ERROR_PROC = (byte) 0xF4;
	public static final byte SLOT_ERROR_DEACT_PROTO = (byte) 0xF3;
	public static final byte SLOT_ERROR_BUSY_AUTOSQ = (byte) 0xF2;
	public static final byte SLOT_ERROR_PIN_TIMEOUT = (byte) 0xF0;
	public static final byte SLOT_ERROR_PIN_CANCEL = (byte) 0xEF;
	public static final byte SLOT_ERROR_BUSY_SLOT = (byte) 0xE0;
	public static final byte SLOT_ERROR_CMD_UNSUP = (byte) 0x00;

	/*
	 * commands
	 */
	public static final byte PC_to_RDR_IccPowerOn = (byte) 0x62;
	public static final byte PC_to_RDR_IccPowerOff = (byte) 0x63;
	public static final byte PC_to_RDR_GetSlotStatus = (byte) 0x65;
	public static final byte PC_to_RDR_XfrBlock = (byte) 0x6F;
	public static final byte PC_to_RDR_Escape = (byte) 0x6B;

	/*
	 * parameters positions
	 */
	public static final byte CCID_IDX_MSG = (byte) 0;
	public static final byte CCID_IDX_LEN = (byte) 1;
	public static final byte CCID_IDX_SLOT = (byte) 5;
	public static final byte CCID_IDX_SEQ = (byte) 6;
	public static final byte CCID_IDX_STATUS = (byte) 7;
	public static final byte CCID_IDX_ERROR = (byte) 8;
	public static final byte CCID_IDX_DATA = (byte) 10;

	/*
	 * internal definitions
	 */
	private static final int CCID_HEADER_SIZE = 10;

	private final byte ccidInterfaceProtocol = 0;
	private final byte ccidProtocol = 0;
	private final int maxIFSD = 0;
	private int ccidFeatures;

	// default size is CCID message size for "Reiner SCT Basis" (see comment below)
	// practical values are also 10 + 261, etc.
	// this value includes 10 bytes header size for bulk transfers and is absolute
	// for control transfers
	private int maxCCIDMsgLen = CCID_HEADER_SIZE + 0x0100;

	private final ByteBuffer ccidBuf;
	private byte sequence = -1;

	/*
	 * create new terminal from device-interface
	 */

	public CCIDTerminal(final UsbBasicDevice device) {
		// initialize device class
		super(device);

		// open device, read descriptors
		super.open();

		// determine capabilities, features, protocols and buffer size
		// from descriptors
		final byte[] rd = super.getRawDescriptors();
		parseDescriptors(rd);

		ccidBuf = ByteBuffer.allocate(maxCCIDMsgLen);
		this.ccidBuf.order(ByteOrder.LITTLE_ENDIAN);
	}

	private void debug(String msg) {
		Log.e("USBCCID", msg);
	}

	/*
	 * parse CCID functional device-interface descriptor and determine capability and communication parameters
	 */

	private void parseDescriptors(final byte[] rd) {
		debug("R-Desc: " + Hex.toString(rd));
		// ReinerSCT RFID basis
		// 12 01 0002 00 00 00 08 4B0C 0291 0100 01 02 00 01, 09 02 5D00 01 01 00 80 32, 09 04 00 00 03 0B 00 00 00, 36 21 1001 00 01 02000000 E8030000 E8030000
		// 01 800A0000 800A0000 01 FE000000 00000000 00000000 B0040400 0A010000 FF FF 0000 00 01, 07058303400005, 07058102400001, 07050202400001

		final ByteBuffer desc = ByteBuffer.wrap(rd);
		desc.order(ByteOrder.LITTLE_ENDIAN);

		int offset = 0;
		while (desc.remaining() > 0) {
			final short len = (short) (desc.get() & 0xFF);
			final byte type = desc.get();

			switch (type) {
			// device descriptor
			case (byte) 0x01:
				break;

			// configuration descriptor
			case (byte) 0x02:
				break;

			// interface descriptor
			case (byte) 0x04:
				// 0 = CCID (bulk transfers), 1 = ICCD version A (control transfers), 2 = ICCD version B (control transfers)
				debug("CCID interface protocol: " + desc.get(desc.position() + 5));
				break;

			// endpoint descriptor
			case (byte) 0x05:
				break;

			// functional descriptor
			case (byte) 0x21:
				debug("CCID version: " + Hex.shortToString(desc.getShort()) + " Slots: " + (desc.get() & 0xFF) + " Voltages: "
						+ Hex.byteToString(desc.get()) + " Protocols: " + Integer.toHexString(desc.getInt()));
				desc.position(desc.position() + 18);
				debug("MaxIFSD: " + Integer.toHexString(desc.getInt()));
				desc.position(desc.position() + 8);

				// exclusive:
				// 0004 0000 = short + ext APDU, 0002 0000 = short APDU, 0001 0000 = TPDU level, 0000 0000 = character level
				ccidFeatures = desc.getInt();
				maxCCIDMsgLen = desc.getInt();

				debug("Features: " + Integer.toHexString(ccidFeatures) + " CCID Msg Len: " + maxCCIDMsgLen);
				break;

			// unknown descriptor type
			default:
				break;
			}

			offset += len;
			desc.position(offset);
		}

	}

	/*
	 * create a CCID request, transfer it
	 */
	private ByteBuffer xferMsg(final byte cmd, final byte p1, final byte p2, final byte p3, final byte[] data, final int offset, final int len) throws IOException {
		final byte slot = 0x00;

		ccidBuf.clear();
		ccidBuf.put(cmd).putInt(len).put(slot).put(++sequence).put(p1).put(p2).put(p3);
		if (data != null && len > 0) {
			ccidBuf.put(data, offset, len);
		}

		debug("<=" + Hex.toString(ccidBuf.array(), 0, ccidBuf.position()));

		super.write(ccidBuf);

		do {
			ccidBuf.clear();
			super.read(ccidBuf);
			debug(">=" + Hex.toString(ccidBuf.array(), ccidBuf.position(), ccidBuf.remaining()));

			// slot = ccidBuf.get(CCID_IDX_SLOT);
			sequence = ccidBuf.get(CCID_IDX_SEQ);
			final byte status = ccidBuf.get(CCID_IDX_STATUS);
			// byte error = ccidBuf.get(CCID_IDX_ERROR);

			if (status == SLOT_STATE_TIMEXT) {
				continue; // wait / try again
			} else if ((status & SLOT_STATE_FAILED) != 0) {
				// return null;
				break; // error occured
			} else {
				return ccidBuf; // all OK
			}
		} while (true);

		// TODO: check!?
		return ccidBuf; // return to higer layer in case of error
	}

	/*
	 * short hand without message data
	 */
	private ByteBuffer xferMsg(final byte cmd, final int p1, final int p2, final int p3) throws IOException {
		return xferMsg(cmd, (byte) p1, (byte) p2, (byte) p3, null, 0, 0);
	}

    private ByteBuffer xferMsg(final byte cmd) throws IOException {
        return xferMsg(cmd, (byte) 0, (byte) 0, (byte) 0, null, 0, 0);
    }

        /**
         * try to connect to card
         *
         * @return ATR of card connected
         */

    public byte[] connect() throws IOException {
		// TODO: powermode = 0 => AUTO Power / = 1 => 5V
		byte powermode = 0x00;  //0; // AUTO power

        // TODO: review
		int i = 0;
		do {
			debug("connect loop: " + (i++));

            final ByteBuffer bb = xferMsg(PC_to_RDR_IccPowerOn, powermode,0 /* RFU */,0 /* RFU */);

            /*
            RDR_to_PC_DataBlock 80h = PC_to_RDR_IccPowerOn, PC_to_RDR_XfrBlock, PC_to_RDR_Secure, RDR_to_PC_SlotStatus
            RDR_to_PC_SlotStatus 81h = PC_to_RDR_IccPowerOff, PC_to_RDR_GetSlotStatus, PC_to_RDR_IccClock, PC_to_RDR_T0APDU, PC_to_RDR_Mechanical, PC_to_RDR_Abort, RDR_to_PC_Parameters
            RDR_to_PC_Parameters 82h = PC_to_RDR_GetParameters, PC_to_RDR_ResetParameters, PC_to_RDR_SetParameters, RDR_to_PC_Escape
            RDR_to_PC_Escape 83h = PC_to_RDR_Escape, RDR_to_PC_DataRateAndClockFrequency
            RDR_to_PC_DataRateAndClockFrequency 84h = PC_to_RDR_SetDataRateAndClockFrequency
             */

			if (bb.get() != (byte) 0x80) { // is RDR_to_PC_DataBlock response type?
				return null;
			}

			// 800500000000000000003B808001010000000000 // all ok
			// 80000000000000410700 // power error
			// 8000000000000141FE00 // card mute

			final byte status = bb.get(CCID_IDX_STATUS); // status
			final byte error = bb.get(CCID_IDX_ERROR); // error
			if (status != 0) {
				if (error == 0x07 && powermode == 0) {
					powermode = (byte) 1; // 5V
					continue;
				} else {
                    final ByteBuffer buf2 = xferMsg(PC_to_RDR_GetSlotStatus);
					debug("SLOTSTATUS: " + Hex.toString(buf2.array(), buf2.position(), buf2.remaining()));

					// wait for slot change
                    final ByteBuffer buffer = ByteBuffer.allocate(2);
                    super.pollInt(buffer);
                    buffer.flip();
                    if(buffer.remaining() > 0) {
                        debug("INT: " + Hex.toString(buffer.array(), buffer.position(), buffer.remaining()));
                        if (buffer.remaining() >= 2 && (buffer.get(1) & 0x03) != 1) {
                            return null;
                        }
                    }

					continue;
				}
			} else {
				final byte[] atr = new byte[bb.getInt(CCID_IDX_LEN)];
				bb.position(CCID_IDX_DATA);
				bb.get(atr);
				debug("Connect / ATR: " + Hex.toString(atr));
				return atr;
			}
		} while (true); // TODO: replace with maxTries? / Thread.sleep(100)
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.vx4.app.mobileIDSE.hal.usb.UsbBasicDevice#close()
	 */
	@Override
	public void close() {
	    if(isOpen()) {
            try {
                final ByteBuffer bb = xferMsg(PC_to_RDR_IccPowerOff);
                debug("power off: " + Hex.toString(bb.array(), bb.position(), bb.remaining()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            super.close();
        }
	}
/*
	// XXX: how to implement for best result?
	public byte[] xferTPDU(final byte[] pdu) throws IOException {
		// ....
		return null;
	}
*/

	/**
	 * transfer a block of protocol data to CCID terminal and read a response block and return it
	 * 
	 * @param pdu
	 * @return response pdu
	 * @throws IOException
	 */
	public byte[] xferAPDU(final byte[] pdu) throws IOException {
		if (pdu == null) {
			return null;
		}

		if (pdu[0] == (byte) 0xFF && pdu[1] == (byte) 0x9A) {
            if(pdu[2] == (byte) 0x01) {
/*
                switch (pdu[3] & 0xFF) {
                    case (byte) 0x01:
                        return "Testvendor".getBytes();
                    case (byte) 0x03:
                        return "Testproduct".getBytes();
                    case (byte) 0x06:
                        return "Testfirmware".getBytes();
                    case (byte) 0x07:
                        return "Testdriver".getBytes();
                }
*/
                // custom stuff
                //final String manufacturer = new String(tpNew.transmit(new byte[] { (byte) 0xFF, (byte) 0x9A, 0x01, 0x01, 0x00 }));
                //final String product = new String(tpNew.transmit(new byte[] { (byte) 0xFF, (byte) 0x9A, 0x01, 0x03, 0x00 }));
                //final String firmwareVersion = new String(tpNew.transmit(new byte[] { (byte) 0xFF, (byte) 0x9A, 0x01, 0x06, 0x00 }));
                //final String driverVersion = new String(tpNew.transmit(new byte[] { (byte) 0xFF, (byte) 0x9A, 0x01, 0x07, 0x00 }));
            } else if(pdu[2] == (byte) 0x04) {
                // PACE, pdu[3] = 0x01 = getCapabilities, 0x02 = EstablishPACE, 0x03 = DestroyPACE, 0x10 = VerifyPIN / ModifyPIN
            }
        }

		/*
		 * write APDU
		 */
		final int maxPayload = maxCCIDMsgLen - CCID_HEADER_SIZE;
		int written = 0;
		short wLevel = 0;

		ByteBuffer bb = null;
		while (written == 0 || pdu.length - written > 0) {
			int current = pdu.length - written;
			if (current > maxPayload) {
				current = maxPayload;
				wLevel = (short) ((wLevel == 0) ? 1 : 3);
			} else {
				wLevel = (short) ((wLevel == 0) ? 0 : 2);
			}

			bb = xferMsg(PC_to_RDR_XfrBlock, (byte) 0x10/* BWI */, (byte) (wLevel), (byte) (wLevel >> 8), pdu, written, current);
			written += current;
			if (wLevel > 1) {
				debug("intermediate read status: " + Hex.toString(bb.array(), bb.position(), bb.remaining()));
			}
		}

		/*
		 * readAPDU
		 */
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		do {
			if (bb.get(CCID_IDX_MSG) != (byte) 0x80) { // is RDR_to_PC_DataBlock response type?
				baos = null;
				break;
			}

			final byte status = bb.get(CCID_IDX_STATUS); // status
			final byte error = bb.get(CCID_IDX_ERROR); // error
			final byte chain = bb.get(CCID_IDX_ERROR + 1); // chain parameter

            debug("S: " + Hex.byteToString(status) + " " + Hex.byteToString(error) + " " + Hex.byteToString(chain));

            // TODO: TEST / REWVIEW
            if(status == 0x02) {
                bb = xferMsg(PC_to_RDR_XfrBlock);
                continue;
                //return null;
            }

			if (chain == (byte) 0x10) {
				/* empty data, read again */
				bb = xferMsg(PC_to_RDR_XfrBlock);
				continue;
			} else if ((status & 0x40) != 0) {
				/* some error occured */
				return null;
			}

			final int len = bb.getInt(CCID_IDX_LEN); // total size of message
			if (len > 0) {
				baos.write(ccidBuf.array(), CCID_IDX_DATA, len);
				if (chain == 0 || chain == 2) {
					break;
				} else {
					bb = xferMsg(PC_to_RDR_XfrBlock, (byte) 0, (byte) 0x10, (byte) 0);
				}
			}
		} while (true);

		return baos != null ? baos.toByteArray() : null;
	}

	@Override
	public String getName() {
		return "[VX4 CCID] " + super.getManufacturer() + " " + super.getProduct();
	}

	private boolean testReinerSCT_RFID() {
		if (super.getVendorID() == 3147 /* Reiner SCT */&& (
				super.getProductID() == 1280 || // final RFID standard (CCID msgLen = 5130, IFSD = FE)
				super.getProductID() == 1281 || // final RFID komfort (CCID msgLen == 5120, IFSD = FE)
				super.getProductID() == 1285 || // wave (CCID msgLen == 5130, IFSD = FE)
				super.getProductID() == 1024 // ref-PKI RFID komfort (CCID msgLen = 270, IFSD = FE)
				)) {
			return true;
		}

		return false;
	}

	@Override
	public boolean hasFeature(final byte feature) {
		if (feature == CCID.FEATURE_EXECUTE_PACE) {
			if (testReinerSCT_RFID()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public byte[] verifyPinDirect(final byte[] PIN_VERIFY) {
		return null;
	}

	@Override
	public byte[] modifyPinDirect(final byte[] PIN_MODIFY) {
		return null;
	}

	@Override
	public byte[] transmitControlCommand(final byte feature, final byte[] ctrlCommand) {
		if (feature == CCID.FEATURE_EXECUTE_PACE && ctrlCommand != null) {
			if (testReinerSCT_RFID()) {
				ByteBuffer bb = ByteBuffer.allocate(6 + ctrlCommand.length);
				bb.order(ByteOrder.LITTLE_ENDIAN);

				bb.putInt(0x01000001); // Reiner SCT Kernel module ID
				bb.putShort((short) 0x00F0); // Reiner SCT CCID Escape do PACE

				bb.put(ctrlCommand);

				final byte[] pace = bb.array();
				try {
					bb = xferMsg(PC_to_RDR_Escape, (byte) 0, (byte) 0, (byte) 0, pace, 0, pace.length);
					if (bb.get(CCID_IDX_MSG) == (byte) 0x83) {
						final int len = bb.getInt(CCID_IDX_LEN);
						final byte status = bb.get(CCID_IDX_STATUS);
						final byte error = bb.get(CCID_IDX_ERROR);
						if (status == (byte) 0) {
							final ByteBuffer pace_res = ByteBuffer.allocate(len);
							pace_res.order(ByteOrder.LITTLE_ENDIAN);

							bb.position(CCID_IDX_DATA);
							bb.limit(bb.position() + len);
							pace_res.put(bb);

							bb = ByteBuffer.allocate(4 + 2 + pace_res.getInt(4));
							bb.order(ByteOrder.LITTLE_ENDIAN);
							bb.putInt(pace_res.getInt(0));
							bb.putShort((short) pace_res.getInt(4));
							pace_res.position(8);
							pace_res.limit(pace_res.position() + pace_res.getInt(4));
							bb.put(pace_res);

							return bb.array();
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public TransportProvider getTransport() {
		try {
			return new TransportProvider() {
				private final byte[] atr = CCIDTerminal.this.connect();

                private int lastSW = 0;

                @Override
                public void close() {
                    CCIDTerminal.this.close();
                }

                @Override
                public Object getParent() {
                    return CCIDTerminal.this;
                }

                //@Override
                public byte[] transmit(final String apdu) {
                    return transmit(Hex.fromString(apdu));
                }

                // XXX: handle GET RESPONSE for T=0 behaviour
                @Override
                public byte[] transmit(final byte[] apdu) {
                    try {
                        this.lastSW = -1;

                        byte[] rpdu = CCIDTerminal.this.xferAPDU(apdu);
                        if (rpdu != null && rpdu.length >= 2) {
                            this.lastSW = ((rpdu[rpdu.length - 2] & 0xFF) << 8) + (rpdu[rpdu.length - 1] & 0xFF);
                            rpdu = ArrayTool.subArray(rpdu, 0, rpdu.length - 2);
                        }

                        return rpdu;
                    } catch (final IOException e) {
                        e.printStackTrace();
                    }

                    return null;
                }

                @Override
                public int lastSW() {
                    return this.lastSW;
                }
            };
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
