/*
 * Copyright 2016-2017 adesso AG
 * Copyright 2011-2016 VX4.NET
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */
package net.vx4.app.mobileIDSE.hal.usb;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import de.persoapp.core.util.ArrayTool;
import de.persoapp.core.util.Hex;

/**
 * @author Christian Kahlo, ck@vx4.de
 */
public class UsbBasicDevice implements Closeable {

	private static final int	USB_TIMEOUT	= 0;

	private final UsbManager	usbMgr;
	private final UsbDevice		usbDev;
	private final UsbInterface	usbIF;

    private UsbDeviceConnection	udc;
    private byte[]				rawDesc;

	private UsbEndpoint			epInt;
	private UsbEndpoint			epIn;
	private UsbEndpoint			epOut;


	UsbBasicDevice(final UsbManager usbMgr, final UsbDevice usbDev, final UsbInterface usbIF) {
        this.usbMgr = usbMgr;
        this.usbDev = usbDev;
        this.usbIF = usbIF;
    }

    protected UsbBasicDevice(final UsbBasicDevice src) {
		if(src == null)
			throw new NullPointerException("No source device.");

        this.usbMgr = src.usbMgr;
        this.usbDev = src.usbDev;
        this.usbIF = src.usbIF;
    }

    public boolean open() {
	    if(this.udc != null) {
	        return true; // already open
        } else {
            this.udc = this.usbMgr.hasPermission(this.usbDev) ? this.usbMgr.openDevice(this.usbDev) : null;
            if(this.udc != null) {
                this.udc.claimInterface(this.usbIF, true);
                this.rawDesc = this.udc.getRawDescriptors();

                for (int i = 0; i < this.usbIF.getEndpointCount(); i++) {
                    final UsbEndpoint usbEP = this.usbIF.getEndpoint(i);

                    if (usbEP.getType() == UsbConstants.USB_ENDPOINT_XFER_INT
                            && usbEP.getDirection() == UsbConstants.USB_DIR_IN) {
                        this.epInt = usbEP;
                    } else if (usbEP.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                        if(usbEP.getDirection() == UsbConstants.USB_DIR_IN) {
                            this.epIn = usbEP;
                        } else if (usbEP.getDirection() == UsbConstants.USB_DIR_OUT) {
                            this.epOut = usbEP;
                        }
                    }
                }


                if (this.epInt != null) {
                    // handle interrupt catches in separate thread? or int-only interfaces?
/*
                    System.out.println("CTRL: " + this.epInt);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final ByteBuffer intBuf = ByteBuffer.allocate(UsbBasicDevice.this.epInt.getMaxPacketSize());
                            while (true) {
                                intBuf.clear();
                                try { pollInt(intBuf); } catch(IOException ioe) { ioe.printStackTrace(); };
                                System.out.println("INT: " + Hex.toString(intBuf.array()));
                                try { Thread.sleep(epInt.getInterval()); } catch (InterruptedException e) { }
                            }
                        }
                    }).start();
*/
                }
                return true;
            } else {
                // open failed?
                // no permission, re-request permission?
            }
        }

        return false;
	}

	public void close() {
		if (isOpen()) {
            this.udc.releaseInterface(this.usbIF);
            this.udc.close();
            this.udc = null;
		}
	}

	public boolean isOpen() {
	    return this.udc != null;
    }

	@Override
	public String toString() {
		return this.getClass().getName() + ": " + Integer.toHexString(this.usbDev.getVendorId()) + "_"
				+ Integer.toHexString(this.usbDev.getProductId()) + " serial: " +
                (this.udc == null ? "[no connection]" : this.udc.getSerial() + " / " + Hex.toString(this.rawDesc));
	}

	private byte[] control(final int requestType, final int request, final int value, final int index, byte[] data) {
		if(data == null || data.length == 0) {
            data = new byte[256];
        }
		final int read = this.udc.controlTransfer(requestType, request, value, index, data, data.length, USB_TIMEOUT);
		if (read < 0) {
			return null;
		}

		return ArrayTool.subArray(data, 0, read);
	}

    private byte[] control(final int requestType, final int request, final int value, final int index) {
	    return control(requestType, request, value, index, null);
    }

	private byte[] getDesc(final int type, final int index) {
	    return control(0x80, 0x06, (type << 8) + index, 0);
	}

	public String getStringDesc(final int index) {
		final byte[] data = getDesc(3, index);
		if (data == null) {
			return null;
		}
		try {
			return new String(data, 2, data[0] - 2, "UTF-16LE");
		} catch (final UnsupportedEncodingException e1) {
			e1.printStackTrace();
			throw new IllegalStateException(e1);
		}
	}

	public int getVendorID() {
		return usbDev.getVendorId();
	}

	public int getProductID() {
		return usbDev.getProductId();
	}

	public String getManufacturer() { return getStringDesc(rawDesc[14]); }

	public String getProduct() {  return getStringDesc(rawDesc[15]); }

	public String getSerialNo() {
		return getStringDesc(rawDesc[16]);
	}

	public byte[] getRawDescriptors() {
		return this.rawDesc.clone();
	}

	// reminder: ur.queue(bb, len) ignores offset, limit & capacity of bb
    private boolean usb(final UsbDeviceConnection dc, final UsbEndpoint ep, final ByteBuffer buf) {
	    if(dc != null && ep != null && buf != null) {
            final UsbRequest ur = new UsbRequest();
            boolean res = ur.initialize(dc, ep) && ur.queue(buf, buf.remaining()) && dc.requestWait() != null;
            ur.close();
            return res;
        }
        throw new NullPointerException();
    }

	protected void write(final ByteBuffer bb) throws IOException {
        bb.flip();
        if(!usb(udc, epOut, bb))
            throw new IOException("USB write error");
	}

	protected void read(final ByteBuffer bb) throws IOException {
        if(!usb(udc, epIn, bb))
            throw new IOException("USB write error");
        bb.flip();
	}

    protected void pollInt(final ByteBuffer bb) throws IOException {
        if(udc != null && epInt != null && bb != null) {
            final UsbRequest ur = new UsbRequest();
            boolean res = ur.initialize(udc, epInt) && ur.queue(bb, bb.remaining());
            while (res && this.udc.requestWait() != ur) {
                try {  Thread.sleep(ur.getEndpoint().getInterval());  } catch (final InterruptedException e) {  break; }
            }
            ur.close();
        }

        bb.flip();
    }
}
