/*
 * Copyright 2016-2017 adesso AG
 * Copyright 2011-2016 VX4.NET
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */
package net.vx4.app.mobileIDSE.hal;

import android.content.Context;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.os.Build;
import android.util.Log;

import com.android.internal.util.Predicate;
import net.vx4.app.mobileIDSE.hal.usb.CCIDTerminal;
import net.vx4.app.mobileIDSE.hal.usb.UsbBasicDevice;
import net.vx4.app.mobileIDSE.hal.usb.UsbDeviceMgr;
import de.persoapp.core.card.TransportProvider;
import de.persoapp.core.util.ArrayTool;
import de.persoapp.core.util.Hex;

import java.util.List;

/**
 * @author Christian Kahlo, ck@vx4.de
 *
 *         This is the HAL (Hardware Abstraction Layer) base class. It's purpose
 *         is to handle hardware / I/O specific parts of the application.
 *
 */

public class HAL {

    public static final String		LOG_TAG					= "HAL";

    private static final int REQUEST_DEVICE    = 0;
    private static final int REQUEST_ENABLE_BT = 1;

    private static final HAL  INSTANCE = new HAL();

    private UsbDeviceMgr mUsbDeviceMgr = null;

    private HAL() {
    }

    /**
     * @param context
     */
    public static void init(final Context context) {
        INSTANCE.mUsbDeviceMgr = UsbDeviceMgr.get(context);
    }

    public static void destroy() {
        INSTANCE.mUsbDeviceMgr.close();
    }

    private TransportProvider openUSB(final byte[] AID) {
        final List<UsbDevice> devs = this.mUsbDeviceMgr.getAccessibleDevices(true, new Predicate<UsbDevice>() {
            @Override
            public boolean apply(final UsbDevice device) {
                if (device.getVendorId() == 1478) {
                    return false;   // ignore Qualcomm chipsets, possible RM NET device
                }
                switch(device.getDeviceClass()) {
                    case UsbConstants.USB_CLASS_PER_INTERFACE:      // quite common
                    case UsbConstants.USB_CLASS_CSCID:              // default
                    case UsbConstants.USB_CLASS_VENDOR_SPEC:        // Axalto CCID smartcard
                        for (int i = 0; i < device.getInterfaceCount(); i++) {
                            final UsbInterface usbIF = device.getInterface(i);  // has a matching interface?
                            switch(usbIF.getInterfaceClass()) {
                                case UsbConstants.USB_CLASS_CSCID:              // real CCID
                                case UsbConstants.USB_CLASS_PER_INTERFACE:      // Axalto bug?
                                case UsbConstants.USB_CLASS_VENDOR_SPEC:        // Reiner SCT fake non-CCID devices
                                    // filter invalid devices
                                    return usbIF.getInterfaceSubclass() != 255 && usbIF.getInterfaceProtocol() != 255;
                                default:
                                    continue;
                            }
                        }
                    default:
                        return false;
                }
            }
        });

        System.out.println("openUSB: " + Hex.toString(AID) + " on readers: " + devs);

        for (final UsbDevice device : devs) {
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                System.out.println("trying device: " + device.getManufacturerName() + " " + device.getProductName() + " " + device.getSerialNumber());
            }

            for (int i = 0; i < device.getInterfaceCount(); i++) {
                final UsbInterface usbIF = device.getInterface(i);
                switch(usbIF.getInterfaceClass()) {
                    case UsbConstants.USB_CLASS_CSCID:              // real CCID
                    case UsbConstants.USB_CLASS_PER_INTERFACE:      // Axalto bug?
                    case UsbConstants.USB_CLASS_VENDOR_SPEC:        // Reiner SCT fake non-CCID devices
                        // filter invalid devices, try to open basic device
                        final UsbBasicDevice bd = usbIF.getInterfaceSubclass() != 255 && usbIF.getInterfaceProtocol() != 255 ?
                                this.mUsbDeviceMgr.open(device, usbIF) : null;
                        final CCIDTerminal ccid = bd != null ? new CCIDTerminal(bd) : null;
                        if (ccid != null) {
                            ccid.open();
                            Log.e(LOG_TAG, "Trying terminal : " + ccid.getManufacturer() + " " + ccid.getProduct());
                            final TransportProvider tp = ccid.getTransport();   // automatically connects to card
                            final byte[] selectAID = {(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x0C, (byte) AID.length};
                            tp.transmit(ArrayTool.arrayconcat(selectAID, AID));
                            if (tp.lastSW() == 0x9000 || tp.lastSW() == 0x6982) {   // XXX: handle GET RESPONSE for T=0 behaviour
                                return tp;
                            }
                            tp.close();     // closes CCID device, too
                        }
                    default:
                        continue;
                }
            }
        }
        return null;
    }

    private TransportProvider openNFC(final byte[] AID) {
        final TransportProvider tp = null; //NfcTransport.getInstance();

        final byte[] selectAID = { (byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x0C, (byte) AID.length };
        tp.transmit(ArrayTool.arrayconcat(selectAID, AID));

        if (tp.lastSW() == 0x9000 || tp.lastSW() == 0x6982) {
            return tp;
        } else {
            tp.close();
        }

        return null;
    }

    // NFC, USB, BT, OMAPI, SE, TEE, PersoSIM
    //org.simalliance.openmobileapi.SEService
    //com.android.nfc_extras.NfcExecutionEnvironment
    //com.ledger.wallet.service.ILedgerWalletService

    public TransportProvider _openTransport(final byte[] aid) {
        TransportProvider tp = openUSB(aid);
        if (tp == null) {
            //tp = openNFC(aid);
        }

        if (tp == null) {
            // tp = openRSCTService(aid);
        }

        if (tp == null) {
            // XXX:
            // tp = new PersoSimTransport();
        }

        return tp;
    }

//    public static List<TransportProvider> listTransports() {
//        final List<TransportProvider> transports = null;
//        return transports;
//    }

    public static TransportProvider openTransport(final byte[] aid) {
        return INSTANCE._openTransport(aid);
    }
}
