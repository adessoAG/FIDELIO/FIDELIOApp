
# FIDELIO App top-level project

Fork of GitLab PersoApp import of GitHub PersoApp repository imported on 2015-03-13 from GoogleCode.

##### Build

- Checkout using 'git clone https://gitlab.com/adessoAG/FIDELIO/FIDELIOApp'
- change dir to client 'cd PersoApp-Android/PersoApp-Client'
- declare android sdk home 'set ANDROID_HOME=_path to your SDK_'
 (permanent solution ist to create a local.properties file within the
  PersoApp-Client directory containing sdk.dir=_path to your SDK_)
- if the project references a -SNAPSHOT release of PersoApp-Core build the
  core at first (see below)
- build & install 'gradlew :app:installRelease'


#### Dependencies

* de.persoapp:persoapp-core:1.0.1 / 1.0.2-SNAPSHOT
* de.persoapp:persoapp-jaxb:1.0.0

SNAPSHOT releases are not committed to git, if you're building a release
depending on a SNAPSHOT release proceed with the following at first:
- change dir to client 'cd PersoApp-Core'
- build & install 'gradlew uploadArchives'


#### previous repositories


https://github.com/PersoApp  
https://code.google.com/archive/p/persoapp/  
https://www.persoapp.de/  
